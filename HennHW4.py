# Justin Henn
# 10/1/2018
# CS4500-E01
# This program is a simulation of a pyramid of numbers with multiple iterations and file input and output
# External files used are HWinfile.txt and HW4hennOutfile.txt
# Python 3.6 used with Pycharm
# Sources:
# 1. https://stackoverflow.com/questions/850795/different-ways-of-clearing-lists
# 2. https://pyformat.info/
# 3. https://stackoverflow.com/questions/3996904/generate-random-integers-between-0-and-9
# 4. https://stackoverflow.com/questions/23294658/asking-the-user-for-input-until-they-give-a-valid-response
# 5. https://docs.python.org/2/library/turtle.html#turtle.clear
# 6. http://book.pythontips.com/en/latest/enumerate.html

# !/usr/bin/python

from random import randint
import turtle


# This function is used to pick a random movement. 0 = up right, 1 = up left,
# 2 = down left, and 3 = down right. The function does not take any parameters.
# The function does return and random integer between 0 and 3. The source
# used for this was number 3.

def random_dice():
    return randint(0, 3)


# This function is used to print information about the game to the screen and to the file.
# The function takes the output file as a parameter and does not return anything.

def print_game_info(outfile):
    game_details = ("This is a game where a 6 layer pyramid is drawn\n"
                    "It then plays through the pyramid until every circle has\n"
                    "been visited. It then prints the game information.\n"
                    "Next it reads a file that will tell it the parameters of the next games\n"
                    "It then goes through those sets of games and prints the game information\n"
                    "for each simulation. At the end it prints the game information for each\n"
                    "full run.\n")
    print(game_details)
    outfile.write(game_details)
    outfile.write("\n")


# This function is used to create the pyramid in a list of lists. It first
# initiates counter z and g and h. It then starts a loop that counts to the
# number of layers. It initializes x and a list called b. It then adds whatever
# number g is to the temporary list b. It increments g and x. Until x is equal to z
# It then increments z and h and appends b to the main "list pyramid" It then
# keeps doing this until the full pyramid is created. It takes the list pyramid
# and the total amount of layers as parameters, and the turtle list and returns nothing.
# It also creates a same list of list that is a list of turtles. This is used to create
# the graphics pyramid

def create_list(full_list, total_numbers, circle_list):
    z = 1
    g = 1
    h = 0

    while h < total_numbers:
        x = 0
        b = []
        q = []
        while x < z:
            b.append(g)
            q.append(turtle.Turtle())
            g += 1
            x += 1
        z += 1
        h += 1
        full_list.append(b)
        circle_list.append(q)
        del (b)
    for i in range(0, total_numbers):
        y = total_numbers + 20 - i * 15
        x = -10 * i
        for d in range(0, len(circle_list[i])):
            circle_list[i][d].hideturtle()
            circle_list[i][d].speed("fastest")
            circle_list[i][d].fillcolor("red")
            circle_list[i][d].penup()
            circle_list[i][d].setposition(x, y)
            circle_list[i][d].begin_fill()
            circle_list[i][d].pendown()
            circle_list[i][d].circle(5)
            circle_list[i][d].end_fill()
            x += 17


# This function is used to print the totals of the runs from the file. It creates a string
# that has the total and average information for all runs in the game. It then
# prints that information. It takes the output file, the total_moves list, the
# total_maxes list, and the number of iterations as parameters and returns nothing

def print_totals(input_file_list, outfile):
    for i, x in enumerate(input_file_list):
        total_info = ("\nRun Number: " + str(i + 1)
                      + "\nNumber of Levels in Run: " + str(input_file_list[i][0])
                      + "\nNumber of Simulations in Run: " + str(input_file_list[i][1])
                      + "\nAverage number of moves for Run: " + str(input_file_list[i][2])
                      + "\nMaximum number of Dots for Run: " + str(input_file_list[i][3]))
        print(total_info)
        outfile.write(total_info)
        outfile.writable("\n")


# This function is used to print the totals of the numbers. It creates a string
# that has the total and average information for a run in the game. It then
# prints that information. It takes the output file, the dot_totals list,
# and the full_list list as parameters and returns nothing

def print_run(outfile, dot_total, full_list):
    run_info = ("Total number of moves: " + str(sum(dot_total))
                + "\nAverage number of dots: " + str(
                (sum(dot_total)) / full_list[len(full_list) - 1][len(full_list) - 1])
                + "\nLargest number of dots: " + str(max(dot_total)) + "\n")
    print(run_info)
    outfile.write(run_info)
    outfile.write("\n")


# This function is used to read the file and put it in a list of lists
# It take the input file and the list and returns nothing

def read_file(infile, input_file_list):
    for line in infile:
        input_file_list.append([int(i) for i in line.strip().split(' ')])


# This function is used to get the color that the circle needs to be painted
# The color is set depending on how many times the circle has been visited
# It takes the pyramid list, x and y coords, and the dot total list
# and returns nothing

def get_color(full_list, x_coord, y_coord, dot_total):
    if dot_total[full_list[x_coord][y_coord] - 1] >= 50:
        new_color = "purple"
    elif dot_total[full_list[x_coord][y_coord] - 1] >= 30:
        new_color = "orange"
    elif dot_total[full_list[x_coord][y_coord] - 1] >= 10:
        new_color = "yellow"
    else:
        new_color = "green"
    return new_color


# This function is used to change the color of the circle
# It take the circle pyramid list, the x and y coordinates
# and the new color to change the circle to. It returns
# nothing. Source number 5 was used for this

def change_color(circle_list, x_coord, y_coord, new_color):
    circle_list[x_coord][y_coord].clear
    circle_list[x_coord][y_coord].fillcolor(new_color)
    circle_list[x_coord][y_coord].begin_fill()
    circle_list[x_coord][y_coord].circle(5)
    circle_list[x_coord][y_coord].end_fill()


# This function is used to reset the circle pyramid back to
# how it was before a game has been played. It takes the circle
# pyramid list and returns nothing. Source number 5 was used for this
# Source 6 was used for this

def reset_dots(circle_list):
    for i, z in enumerate(circle_list):
        for g, h in enumerate(circle_list[i]):
            circle_list[i][g].clear
            circle_list[i][g].fillcolor("red")
            circle_list[i][g].begin_fill()
            circle_list[i][g].circle(5)
            circle_list[i][g].end_fill()


# This function is used to clear the circle pyramid list
# so a new size pyramid list can be created. It takes the
# circle pyramid list and returns nothing. Source 6
# was used for this

def clear_list(circle_list):
    for i, z in enumerate(circle_list):
        for g, h in enumerate(circle_list[i]):
            circle_list[i][g].clear()


# This function is used to get the max dots and the average for the run done
# by a line in the file read in. It take the total list, the file list, and
# what row we are working in the file list. It returns nothing Source 6
# was used for this

def get_max_and_average(list_of_totals, input_file_list, what_row_in_input_file_list):
    total = 0
    for i, x in enumerate(list_of_totals):
        total += list_of_totals[i][0]
    avg = total / len(list_of_totals)
    input_file_list[what_row_in_input_file_list].append(avg)
    y = max(map(lambda x: x[1], list_of_totals))
    input_file_list[what_row_in_input_file_list].append(y)


# This funciton is used for the pyramid traversal. It first creates dot_total
# which is a list to keep the total amount of times a number has been visited
# per number. done_checker is used to see if every dot has been visited. The
# first dot_total element is then set to 1 as when the game starts the dot is
# moved to one. The x_coord and y_coord variables are used to traverse the lists
# Right now they are initialized to 0,0 because we are at the number 1 which is
# in the first list, first element, of the full_list. The x_coord shows what list
# we are in, and the y_coord shows what element we are on. It then prints that info.
# Next it checks to see if done_checker is 0. If not then it loops through the
# the dot_total list to see if everything has been visited. If not it will keep
# done_checker set to 0. It then checks to see if done_checker is 0 and if not
# it rolls a random number using the function. It then checks to see if that
# number is 0. If it is then we know we are going up right. THat means we will
# be subtracting one from the x_coord and from the y_coord, since we are going
# up one list, and back one element. We know if both the x_coord_and y_coord will
# be less than 0 if we do the subtraction that means we cannot move. The if statement
# checks that if the subtraction will be an issue then it just prints the current
# spot we are at since we couldn't move. Else, we update the coords and print where
# we went. Either way we also update the visit totals. If the random number is
# 1 then that means up right. That means we subtract 1 from x_coord and we add one
# to y_coord. We check in the if statement to see if minus 1 from the x_coord will
# be less than 0 and if the y_coord is already set to the last element of the list
# we will be going to. If that is so then we can't move and just print where we
# are at. If it okay, then we will subtract one from x_coord and print our new
# location. It will also updates the visit totals as well. It then checks to see
# if the random number is 2. That would mean to move down left. That means moving
# down one list and to the left. It checks to see if x_coord gets updated by one
# if that will go past the amount of lists in the full_list. If that happens then
# the dot stays where it's at. If not then it updates x_coord and prints the new
# number. It also updates the visit totals. Finally, if the random number is 3
# that means the dot moves down and right. It first checks to see if adding one
# to the x_coord will be out of bounds for the number of lists. If not, then it
# it checks to see if y_coord will be beyond the number of elements in the list
# it is moving to. If either condition is found the it stays where it is at.
# If not then it will move and priunt the new number. It will update visit totals.
# This loop is done until every number has been visited. It then prints the totals.
# The function takes the output file, the full_list pyramid, and the total_numbers
# which represents the number count in the pyramid. The function does not return
# anything.

def pyramid(outfile, full_list, total_numbers, circle_list):
    dot_total = [0] * total_numbers
    return_list = [0] * 2
    done_checker = 0
    dot_total[0] = 1
    x_coord = 0
    y_coord = 0
    change_color(circle_list, x_coord, y_coord, "blue")
    while done_checker == 0:
        for i in range(0, total_numbers):
            if dot_total[i] == 0:
                done_checker = 0
                break
            else:
                done_checker = 1
        if done_checker == 0:
            random_num = random_dice()
            if random_num == 0:
                if (x_coord - 1) < 0 or (y_coord - 1) < 0:
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                else:
                    color_change = get_color(full_list, x_coord, y_coord, dot_total)
                    change_color(circle_list, x_coord, y_coord, color_change)
                    x_coord = x_coord - 1
                    y_coord = y_coord - 1
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                    change_color(circle_list, x_coord, y_coord, "blue")
            elif random_num == 1:
                if (x_coord - 1) < 0 or y_coord > (len(full_list[x_coord - 1]) - 1):
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                else:
                    color_change = get_color(full_list, x_coord, y_coord, dot_total)
                    change_color(circle_list, x_coord, y_coord, color_change)
                    x_coord = x_coord - 1
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                    change_color(circle_list, x_coord, y_coord, "blue")
            elif random_num == 2:
                if (x_coord + 1) > (len(full_list) - 1):
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                else:
                    color_change = get_color(full_list, x_coord, y_coord, dot_total)
                    change_color(circle_list, x_coord, y_coord, color_change)
                    x_coord = x_coord + 1
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                    change_color(circle_list, x_coord, y_coord, "blue")
            else:
                if (x_coord + 1) > (len(full_list) - 1) or (y_coord + 1) > (len(full_list[x_coord + 1]) - 1):
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                else:
                    color_change = get_color(full_list, x_coord, y_coord, dot_total)
                    change_color(circle_list, x_coord, y_coord, color_change)
                    x_coord = x_coord + 1
                    y_coord = y_coord + 1
                    dot_total[full_list[x_coord][y_coord] - 1] += 1
                    change_color(circle_list, x_coord, y_coord, "blue")
    print_run(outfile, dot_total, full_list)
    return_list[0] = sum(dot_total)
    return_list[1] = max(dot_total)
    return return_list


# This is the "main portion of the program. It firsts opens the output file.
# It then creates a blank full_list, and sets the total amount of numbers to 21.
# It calls the create_list function and passes the 2 arguments to create the
# pyramid. It then prints the game information using print_game_info. It next
# calls the pyramid function sending the outpfile, the full_list "pyramid",
# and the total_numbers which are how many numbers are in the pyramid.
# After that it closes the file ending the program.

num_layers = 6
initoutfile = open("HW4hennOutfile.txt", 'w')
initfile = open("HWinfile.txt")
full_list = []
circle_list = []
input_file_list = []
create_list(full_list, num_layers, circle_list)
total_numbers = full_list[len(full_list) - 1][-1]
print_game_info(initoutfile)
turtle.hideturtle()
pyramid(initoutfile, full_list, total_numbers, circle_list)
reset_dots(circle_list)
read_file(initfile, input_file_list)
for i in range(0, len(input_file_list)):
    full_list.clear()
    clear_list(circle_list)
    circle_list.clear()
    create_list(full_list, input_file_list[i][0], circle_list)
    total_numbers = full_list[len(full_list) - 1][-1]
    list_of_totals = [[0, 0] for i in range(input_file_list[i][1])]
    for z in range(0, input_file_list[i][1]):
        list_of_totals[z] = pyramid(initoutfile, full_list, total_numbers, circle_list)
        reset_dots(circle_list)
    get_max_and_average(list_of_totals, input_file_list, i)
print_totals(input_file_list,initoutfile)

turtle.getscreen()._root.mainloop()

initoutfile.close()
initfile.close()